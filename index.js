// "require" directive - used to load node.js modules
//  "https module" - lets the node.js transfer data using the HTTP
// "HTTP" - protocol that allows the fetching of resources
let http = require("http");

// createServer()method - used to create an HTTP server that listens to requests on a specified port and gives responses back to the client.
// request - messages sent by the client, usually a web browser
// response - messages sent by the server as an answer
http
  .createServer(function (request, response) {
    // writeHead() method - used to set a status code for the response and set the content-type of the response
    response.writeHead(200, { "Content-Type": "text/plain" });
    // send the response with the text content "hello world"
    response.end("hello world");
  })

  //   The server will be assigned to port 4000 via "listen(4000)" - method where the server will listen to any requests that are sent to it eventually communicating with our server.
  .listen(4000);

console.log("Server running at localhost:4000");
